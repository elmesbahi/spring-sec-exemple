package org.afpa.web.services;

import org.afpa.web.dto.MessageDto;
import org.springframework.data.domain.PageRequest;

public interface MessageService {

	Iterable<MessageDto> findAll(PageRequest pageRequest);

	void save(MessageDto message);

	void deleteMessage(Long id);

	MessageDto findById(Long id);

}
