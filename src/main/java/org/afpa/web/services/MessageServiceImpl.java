package org.afpa.web.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.afpa.web.dto.MessageDto;
import org.afpa.web.entities.MessageEntity;
import org.afpa.web.repositories.MessageRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public Iterable<MessageDto> findAll(PageRequest currentPage) {
		List<MessageDto> res = new ArrayList<>();
		messageRepository.findAll(PageRequest.of(currentPage.getPageNumber(), currentPage.getPageSize())).forEach(e->res.add(modelMapper.map(e, MessageDto.class)));
		return res;
	}

	@Override
	public void save(MessageDto message) {
		MessageEntity msgEntity = modelMapper.map(message,MessageEntity.class);
		msgEntity = messageRepository.save(msgEntity);
		message.setId(msgEntity.getId());
	}

	@Override
	public void deleteMessage(Long id) {
		messageRepository.deleteById(id);
	}

	@Override
	public MessageDto findById(Long id) {
		Optional<MessageEntity> findById = messageRepository.findById(id);
		MessageDto msg = modelMapper.map(findById.get(),MessageDto.class);
		return msg;
	}

}
