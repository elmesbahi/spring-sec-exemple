package org.afpa.web.controllers;

import javax.validation.Valid;

import org.afpa.web.dto.MessageDto;
import org.afpa.web.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MessageController {

	@Autowired
	private MessageService messageService;

	@GetMapping("/list")
	public ModelAndView list() {
		Iterable<MessageDto> messages = this.messageService.findAll(PageRequest.of(0, 5));
		return new ModelAndView("messages/list", "messages", messages);
	}

	@GetMapping("/view/{id}")
	public ModelAndView view(@PathVariable("id") String id) {
		MessageDto message = messageService.findById(Long.valueOf(id));
		return new ModelAndView("messages/view", "message", message);
	}

	@GetMapping("/create")
	public ModelAndView createForm() {
		return new ModelAndView("messages/form", "message", MessageDto.builder().build());
	}

	@PostMapping("/create")
	public ModelAndView create(@Valid MessageDto message, BindingResult result,
			RedirectAttributes redirect) {
		if (result.hasErrors()) {
			return new ModelAndView("messages/form", "formErrors", result.getAllErrors());
		}
		this.messageService.save(message);
		redirect.addFlashAttribute("globalMessage", "view.success");
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/view/"+message.getId());
		return mav;
	}

	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		this.messageService.deleteMessage(id);
		Iterable<MessageDto> messages = this.messageService.findAll(PageRequest.of(0, 5));
		return new ModelAndView("messages/list", "messages", messages);
	}

	@GetMapping("/modify/{id}")
	public ModelAndView modifyForm(@PathVariable("id") String id) {
		MessageDto message = messageService.findById(Long.valueOf(id));
		return new ModelAndView("messages/form", "message", message);
	}
	
	@GetMapping("/login")
	public void login() {
	}

}
