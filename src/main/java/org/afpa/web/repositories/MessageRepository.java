package org.afpa.web.repositories;

import org.afpa.web.entities.MessageEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MessageRepository extends PagingAndSortingRepository<MessageEntity, Long> {

}
