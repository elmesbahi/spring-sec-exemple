package org.afpa.web.repositories;

import org.afpa.web.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
	public UserEntity findByUserName(String username);
}
