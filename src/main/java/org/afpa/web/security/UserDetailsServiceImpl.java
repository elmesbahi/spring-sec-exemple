package org.afpa.web.security;

import org.afpa.web.entities.UserEntity;
import org.afpa.web.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = userRepository.findByUserName(username);
		if (null == user) {
			throw new UsernameNotFoundException("Nousernamed" + username);
		} else {
			return new UserDetailsImpl(user);
		}
	}
}
