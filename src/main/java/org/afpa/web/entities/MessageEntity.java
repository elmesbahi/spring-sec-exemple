package org.afpa.web.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="messages")
public class MessageEntity {
	@Id
	@GeneratedValue
	Long id;
	String text;
	String summary;
	Date created = new Date();
}
