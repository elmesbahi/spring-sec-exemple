package org.afpa.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/create", "/delete/**", "/view/**").hasRole("ADMIN")
				.antMatchers("/list").hasAnyRole("ADMIN", "USER")
				.and().formLogin().loginPage("/login").permitAll()
				;
	}
	
	public static void main(String[] args) {
		System.out.println("pwd1 = <"+BCrypt.hashpw("pwd1", BCrypt.gensalt())+">");
		System.out.println("pwd2 = <"+BCrypt.hashpw("pwd2", BCrypt.gensalt())+">");
		System.out.println("pwd3 = <"+BCrypt.hashpw("pwd3", BCrypt.gensalt())+">");
	}

}
